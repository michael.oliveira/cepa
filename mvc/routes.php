<?php

\Mvc\Router::add("home/index", "HomeController", "index");
\Mvc\Router::add("home/sobre", "HomeController", "sobre");
\Mvc\Router::add("cliente/index", "ClienteController", "index");
\Mvc\Router::add("cliente/add", "ClienteController", "add");
\Mvc\Router::add("cliente/save", "ClienteController", "save");
\Mvc\Router::add("cliente/edit", "ClienteController", "edit");
\Mvc\Router::add("cliente/delete", "ClienteController", "delete");
\Mvc\Router::add("produto/index", "ProdutoController", "index");