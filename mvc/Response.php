<?php
namespace Mvc;
chdir( dirname(__DIR__) );

class Response {
    private $xmlHttpRequest = false;
    
    public function render($target, $view, $params = [])
    {
        if (!$this->getXmlHttpRequest()){
            
            foreach ($params as $key => $value) {
                $$key = $value;
            }
            

            include "views/$target/" . $view . ".php";
            
        } else {
            
            $html = file_get_contents("views/$target/" . $view . ".php");
            libxml_use_internal_errors(true);
            $dom = new \DOMDocument();
            $dom->loadHTML($html);

        return $dom;
        }
        
    }
    
    public function setXmlHttpRequest($xmlRequest){
        $this->xmlHttpRequest = $xmlRequest;
    }
    
    public function getXmlHttpRequest(){
        return $this->xmlHttpRequest;
    }
}
