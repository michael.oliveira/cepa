<?php
namespace Mvc;

class Database {
    
    public function __construct() {}
    
    public static function query($sql, $params = []){
        $stm = static::connection()->prepare($sql);
        $stm->execute($params);
        
        $result = $stm->fetch();
        
        return $result;
    }
    
    public static function fetchAll($sql){
        $stm = static::connection()->query($sql);
        
        $result = $stm->fetchAll();
        
        return $result;
    }
    
    public static function connection(){
        try {
            $con = new \PDO("mysql:host=127.0.0.1;dbname=venda", "root", "Fms237691");
            $con->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
        return $con;
    }
    
    public static function insert($table, $post){
        
    try {
        
        $campos = implode(", ", array_keys($post));
        
        $data = implode(',', array_map(function($x) {
            if(!is_float($x)){
                return "'" . $x . "'";
            } else {
                return $x;
            }
        }, $post));
 
        $sql = "INSERT INTO $table ($campos) VALUES (" . $data. ")";
        $query = static::connection()->prepare($sql);
        $query->execute();

    } catch (Exception $ex) {
        echo "Ao ao tentar inserir " . $ex->getMessage();
        
    }
    }
    
}