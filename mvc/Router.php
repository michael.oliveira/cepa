<?php
namespace Mvc;

class Router {
    private static $routes = [];
    
    private function __construct() {}
    
    public static function add($route, $controller, $method){
        static::$routes[$route] = array(
            "controller"    =>  $controller,
            "method"    =>  $method
        );
    }
    
    public static function getAction($route){
        
        if (array_key_exists($route, static::$routes))
        {
            return static::$routes[$route];
        } else {
            throw new Exception("Este caminho $route nao foi localizado");
        }
    }
    
}
