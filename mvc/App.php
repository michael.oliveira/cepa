<?php
namespace Mvc;

session_start();

use Venda\Model\Repository\ProdutoRepositoryPDO;
use Venda\Model\Repository\PedidoRepositoryPDO;
use Mvc\Database;
use Venda\Model\Repository\ClienteRepositoryPDO;
use Venda\Model\Repository\ItemRepositoryPDO;

class App {
    protected $controller = "HomeController";
    protected $method = "indexAction";
    protected $params = [];
    
    public function __construct() {
        $pdo = Database::connection();
        $produto = new ProdutoRepositoryPDO($pdo);
        $pedido = new PedidoRepositoryPDO($pdo);
        $cliente = new ClienteRepositoryPDO($pdo);
        $item = new ItemRepositoryPDO($pdo);
        $url = static::parseUrl();
        
        $controllerName = ucfirst(strtolower($url[3])) . "Controller";

        if (file_exists(APP_PATH . "Controller/".$controllerName.".php")){
            $this->controller = $controllerName;
            unset($url[3]);
        }

        require APP_PATH . "Controller/".$controllerName.".php";
        $controller = "\Venda\Controller\\" . $this->controller;
        
        switch ($controllerName){
            case 'ProdutoController':
                $this->controller = new $controller($produto);
                break;
            case 'PedidoController':
                $this->controller = new $controller($produto, $pedido, $cliente, $item);
                break;
            case 'ClienteController':
                $this->controller = new $controller($cliente);
                break;
            case 'HomeController':
                $this->controller = new $controller($pedido);
                break;
            default:
                $this->controller = new $controller;
        }
        
        if (isset($url[4])){
            $methodName = strtolower($url[4])."Action";
            
            if (method_exists($this->controller, $methodName)){
                $this->method = ($methodName) ? $methodName : "indexAction";
                unset($url[4]);
            }
        }
        
        $this->params = $url ? array_values($url) : $this->params;

        return \call_user_func_array([$this->controller, $this->method], $this->params);
        
    }
    
    public static function parseUrl(){
        $url = $_SERVER['REQUEST_URI'];

        if (isset($url)){
            return explode("/" , filter_var(rtrim($url, "/"), FILTER_SANITIZE_URL));
            return $url;
        }
    }
}
