<?php

namespace Mvc;

use Mvc\Database;
use Mvc\Response;

class Controller {
    protected $params;
    private $con;
    protected $redirect;
    private $response;
    
    public function __construct() {
        $this->params = func_get_args();
        $this->con = new Database();
        $this->response = new Response();
    }
    
    public function getConnection(){
        return $this->con;
    }
    
    public function getParams(){
        return $this->params;
    }
    
    public function getRedirect(){
        return $this->redirect;
    }
    
    public function setRedirect($redirect){
        $this->redirect = $redirect;
    }
    
    public function getResponse(){
        return $this->response;
    }
}
