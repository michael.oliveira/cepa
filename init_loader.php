<?php
chdir( dirname(__DIR__) );

define("SYS_PATH", "mvc/");
define("APP_PATH", "src/Venda/");
define('ROOT_PATH', "/public/");
    
require_once "vendor/autoload.php";
require_once SYS_PATH . "routes.php";
require_once SYS_PATH . "Router.php";
require_once SYS_PATH . "Response.php";

$app = new \Mvc\App();
    