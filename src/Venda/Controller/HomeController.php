<?php
namespace Venda\Controller;

use Venda\Model\Pedido;
use Mvc\Controller;
use Venda\Model\Repository\PedidoRepository;

class HomeController extends Controller{
    
    private static $target = 'home';
    protected $redirect = '/venda/public/';
    private $pedido;
    
    public function __construct(PedidoRepository $pedido) {
        parent::__construct();
        $this->pedido = $pedido;
    }
    public function indexAction()
    {
        $pedido = $this->pedido->getPedidos();

        $this->getResponse()->render(self::$target, "index", 
            array(
                "rows"  => $pedido,
                "redirect"  => $this->getRedirect()
            )
        );
    }
    
    public function sobreAction(){
        echo "estamos no aqui";
    }
}
