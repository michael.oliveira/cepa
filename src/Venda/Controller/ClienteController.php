<?php
namespace Venda\Controller;

use Mvc\Controller;
use Venda\Model\Cliente;
use Venda\Model\Repository\ClienteRepository;

class ClienteController extends Controller {
    
    private static $target = 'cliente';
    protected $redirect = '/venda/public/cliente';
    private $cliente;
    
    public function __construct(ClienteRepository $cliente) {
        parent::__construct();
        $this->cliente = $cliente;

        
    }
    
    public function indexAction(){

        $cliente = $this->cliente->getClientes();
        
        $this->getResponse()->render(self::$target, "index", 
            array(
                "rows"  => $cliente
            )
        );
    }
    
    public function addAction(){
        $this->getResponse()->render(
                self::$target, 
                "add",
                array(
                    'redirect'  => $this->getRedirect()
                ));
    }
    
    public function editAction(){
        $args = func_get_args();
        $id = (int) $args[3];
        $cliente = $this->cliente->getCliente($id);

        $this->getResponse()->render(
                self::$target, 
                "edit", 
                array(
                    'id'    =>  $id,
                    'cliente'   => $cliente,
                    'redirect'  => $this->getRedirect()
            ));
    }
    
    public function deleteAction(){
        $args = func_get_args();
        $id = (int) $args[3];
        
        try {
            Cliente::delete($id);
            
            header('location: ' . $this->getRedirect());
            
        } catch (\PDOException $ex) {
            echo $ex->getMessage();
        }
    }
    
    public function saveAction(){
        $id = (int) func_get_arg(3);
        
        if(!$id){
            $this->cliente->insertCliente($_POST);
            header('location: ' . $this->getRedirect());
        } else {
            $this->cliente->updateCliente($id, $_POST);
            header('location: ' . $this->getRedirect());
        }
    }
}
