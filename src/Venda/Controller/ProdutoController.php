<?php
namespace Venda\Controller;

use Mvc\Controller;
use Mvc\Database;
use Venda\Model\Repository\ProdutoRepository;

class ProdutoController extends Controller {
    
    private static $target = 'produto';
    private $produto;
    protected $redirect = '/venda/public/produto';
    
    public function __construct(ProdutoRepository $produto) {
        parent::__construct();
        $this->produto = $produto;
        $this->params = func_get_args();
    }
    
    public function indexAction(){

        $produto = $this->produto->getProdutos();
        
        $this->getResponse()->render(self::$target, "index", 
            array(
                "rows"  => $produto
            )
        );
    }
    
    public function addAction(){
        $sql = "SELECT
                *
                FROM tipo";
        $query = Database::connection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);        

        $this->getResponse()->render(self::$target, "add",
                array(
                    'tipos' => $result
                ));
    }
    
    public function editAction(){
        $id = (int) func_get_arg(3);
        
        $sql = "SELECT
                *
                FROM tipo";
        $qryTipos = Database::connection()->prepare($sql);
        $qryTipos->execute();
        $result = $qryTipos->fetchAll(\PDO::FETCH_ASSOC); 

        $produto = $this->produto->getProduto($id);

        $this->getResponse()->render(self::$target, "edit", 
            array(
                'id'    =>  $id,
                'tipos' => $result,
                'produto'   => $produto
            ));
    }

    public function deleteAction(){
        $id = func_get_arg(3);
        
        if(isset($id)){
            try {
            
                $this->produto->delete($id);

                header('location: ' . $this->getRedirect());

            } catch (\PDOException $ex) {
                echo $ex->getMessage();
            }
        }

    }
    
    public function saveAction(){
        extract($_POST);

        if(!func_get_arg(3)){
            $this->produto->insertProduto($_POST);
        } else {
            $this->produto->updateProduto(func_get_arg(3), $_POST);
        }
        
        header('location: ' . $this->getRedirect());
    }
}
