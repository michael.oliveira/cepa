<?php
namespace Venda\Controller;

use Venda\Model\Pedido;
use Venda\Model\ItemPedido;
use Venda\Model\Produto;
use Mvc\Controller;
use Mvc\Response;
use Mvc\Database;
use Venda\Model\Repository\ProdutoRepository;
use Venda\Model\Repository\PedidoRepository;
use Venda\Model\Repository\ClienteRepository;
use Venda\Model\Repository\ItemRepository;

class PedidoController extends Controller {
    
    private $pedido;
    private $produto;
    private $cliente;
    private $items;
    protected $redirect = '/venda/public/pedido';
    
    private static $target = 'pedido';
    
    public function __construct(ProdutoRepository $produto, PedidoRepository $pedido, ClienteRepository $cliente, ItemRepository $items) {
        parent::__construct();
        $this->pedido = $pedido;
        $this->produto = $produto;
        $this->cliente = $cliente;
        $this->items = $items;
    }
    
    public function indexAction(){
        
        $pedidos = $this->pedido->getPedidos();

        $this->getResponse()->render(self::$target, "index", 
            array(
                "pedidos"  => $pedidos,
                "redirect"  => $this->redirect
            )
        );
    }
    
    public function exibirAction(){
        $id = (int )func_get_arg(3);
        $pedido = $this->pedido->getPedido($id);
        $items = $this->items->getPedidoItems($id);
        $cliente = $this->cliente->getCliente($pedido->getCodCliente());

        $this->getResponse()->render(self::$target, "exibir", 
            array(
                'pedido'  => $pedido,
                'items' => $items,
                'cliente'   => $cliente,
                'redirect'  => $this->redirect
            )
        );
    }
    
    public function concluirAction(){
        $clientes = $this->cliente->getClientes();

        $this->getResponse()->render(self::$target, "concluir", 
            array(
                'pedido'  => $this->pedido,
                'redirect'  => $this->redirect,
                'clientes'  => $clientes
            )
        );
    }
    
    public function itemsAction(){
        $pedido = $this->pedido;

        $this->getResponse()->render(self::$target, "items", 
            array(
                "rows"  => $pedido,
                "redirect"  => $this->redirect
            )
        );
    }
    
    public function addAction(){
        $id = (int )func_get_arg(3);

        if (isset($id) && preg_match("/^[0-9]+/", $id)){
            $produto = $this->produto->getProduto($id);
            $item = new ItemPedido();
            $item->setProduto($produto);
            $item->setQtd(1);
            $this->pedido->addItem($item);
        }
        header('location: /venda/public/pedido/items');
         
    }
    
    public function updateAction(){
        $id = (int) $_POST['cod_produto'];

        if (isset($id) && preg_match("/^[0-9]+/", $id)){
            $produto = $this->produto->getProduto($id);
            $item = new ItemPedido();
            $item->setProduto($produto);
            $item->setQtd($_POST['qtd']);
            $this->pedido->updateItem($item);
        }
        header('location: /venda/public/pedido/items');
    }
   
    public function editAction(){
        $id = (int )func_get_arg(3);
        $sql = "SELECT
                *
                FROM tipo";
        
        $qryTipos = Database::connection()->prepare($sql);
        $qryTipos->execute();
        $result = $qryTipos->fetchAll(\PDO::FETCH_ASSOC); 

        $query = Produto::find($id);

        Response::render(self::$target, "edit", 
            array(
                'id'    =>  $id,
                'tipos' => $result,
                'produto'   => $query
            ));
    }
    
    public function removeitemAction(){
        $id = (int)func_get_arg(3);

        if (isset($id) && preg_match("/^[0-9]+/", $id)){
            $this->pedido->removeItem($id);
        }
        header('location: /venda/public/pedido/items');
    }
    
    public function deleteAction(){
        $id = (int)func_get_arg(3);

        if (isset($id) && preg_match("/^[0-9]+/", $id)){
            $this->pedido->deletePedido($id);
        }
        header('location: /venda/public/pedido/index');
    }
    
    public function limparAction(){
        $this->pedido->limpaItems();
        
        header('location: /venda/public/pedido/items');
    }
    public function saveAction(){
    try {
        $params = func_get_args();
        $id = (isset($params[3])) ? $params[3] : null;

        $cliente = $this->cliente->getCliente($_POST['cod_cliente']);
            $dados = array(
                'data_pedido'   => date("Y-m-d"),
                'cod_cliente'   => $_POST['cod_cliente']
            );
        $pdo = $this->pedido->getPdo();
        if(!$id){
            $this->pedido->insertPedido($dados);
            
            $codPedido = $pdo->lastInsertId();

            foreach($this->pedido->getPedidoItems() as $item){
                $produto = $item->getProduto()->getCodProduto();
                $qtd = $item->getQtd();
                $stmt = $pdo->prepare("INSERT INTO item_venda(cod_pedido, cod_produto, qtd)
                        VALUES(:pedido, :produto, :qtd)");
                $stmt->bindParam('pedido', $codPedido);
                $stmt->bindParam('produto', $produto);
                $stmt->bindParam('qtd', $qtd);  
                $stmt->execute();
                //unset($this->pedido->getPedidoItems());
            }
                $this->pedido->limpaItems();
                unset($_SESSION['pedido']);
                header('location: /venda/public/pedido/index');
            } else {
                $this->pedido->updatePedido($id, $dados);
        }
    } catch(\PDOException $ex){
        echo "<div class=\"alert alert-danger\" role=\"alert\"> Erro:". $ex->getMessage() . "</div>  ";
    }
    }
}
