<?php

namespace Venda\Model\Repository;

class ClienteRepositoryPDO implements ClienteRepository {
    protected $redirect;
    
    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }
    
    public function getClientes() {
        $stmt = $this->pdo->prepare("SELECT * FROM cliente");
        $stmt->setFetchMode(\PDO::FETCH_CLASS, 'Venda\Model\Cliente');
        $stmt->execute();
        
        return $stmt->fetchAll();
    }
    
    
    public function deleteCliente($id){
        $stmt = $this->pdo->prepare("DELETE FROM cliente WHERE cod_cliente = :id");
        $stmt->bindParam('id', $id);
        $stmt->execute();
    }
    
    public function setRedirect($redirect){
        $this->redirect = $redirect;
    }
    
    public function getRedirect(){
        return $this->redirect;
    }

    public function getCliente($id) {
        $stmt = $this->pdo->prepare("SELECT * FROM cliente WHERE cod_cliente = :id");
        $stmt->bindParam('id', $id);
        
        $stmt->execute();
        
        return $stmt->fetchObject('Venda\Model\Cliente');
    }

    public function insertCliente($dados) {
        try {
        
        $campos = implode(", ", array_keys($dados));
        
        $data = implode(',', array_map(function($x) {
            if(!is_float($x)){
                return "'" . $x . "'";
            } else {
                return $x;
            }
        }, $dados));
 
        $sql = "INSERT INTO cliente ($campos) VALUES (" . $data. ")";
        $query = $this->pdo->prepare($sql);
        $query->execute();

    } catch (Exception $ex) {
        echo "Ao ao tentar inserir " . $ex->getMessage();
        
    }
    }

    public function updateCliente($id, $dados) {
        try {
            $data = implode(", ", array_map(function($x, $v){
                
                return $x . " = \"" . $v . "\"";

            }, array_keys($dados), array_values($dados)));
            
            $sql = "UPDATE cliente SET $data WHERE cod_cliente = :id";
            
            $query = $this->pdo->prepare($sql);
            $query->bindValue(':id', $id);
            $query->execute();

        } catch (Exception $ex) {
            echo "Ao ao tentar atualizar " . $ex->getMessage();
        
        }
    }

}
