<?php

namespace Venda\Model\Repository;

class ProdutoRepositoryPDO implements ProdutoRepository {
    private $pdo;
    
    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }
    public function getProduto($id) {
        $stmt = $this->pdo->prepare("SELECT * FROM produto WHERE cod_produto = :id");
        $stmt->bindParam('id', $id);
        
        $stmt->execute();
        
        return $stmt->fetchObject('Venda\Model\Produto');
    }

    public function getProdutos() {
        $stmt = $this->pdo->prepare("SELECT * FROM produto");
        $stmt->setFetchMode(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE, 'Venda\Model\Produto');
        $stmt->execute();
        
        $produtos = $stmt->fetchAll();
        $arr = array();
        $query = $this->pdo->prepare('select * from tipo where cod_tipo = :id');
        foreach($produtos as $produto) {

            $query->setFetchMode(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE, 'Venda\Model\Tipo');
            $query->execute(array('id' => $produto->getTipoId()));
            $tipo = $query->fetch();
            $produto->setTipoProduto($tipo);
            $arr[] = $produto;
        }
        
        return $arr;
    }
    
    public function updateProduto($id, $dados){
        try {
            $data = implode(", ", array_map(function($x, $v){
                
                return $x . " = \"" . $v . "\"";

            }, array_keys($dados), array_values($dados)));
            
            $sql = "UPDATE produto SET $data WHERE cod_produto = :id";
            
            $query = $this->pdo->prepare($sql);
            $query->bindValue(':id', $id);
            $query->execute();

        } catch (Exception $ex) {
            echo "Ao ao tentar inserir " . $ex->getMessage();
        
        }
    }
    
    public function delete($id){
        $stmt = $this->pdo->prepare("DELETE FROM produto WHERE cod_produto = :id");
        $stmt->bindParam('id', $id);
        $stmt->execute();
    }
    
    public function insertProduto($dados){
        try {
        
        $campos = implode(", ", array_keys($dados));
        
        $data = implode(',', array_map(function($x) {
            if(!is_float($x)){
                return "'" . $x . "'";
            } else {
                return $x;
            }
        }, $dados));
 
        $sql = "INSERT INTO produto ($campos) VALUES (" . $data. ")";
        $query = $this->pdo->prepare($sql);
        $query->execute();

    } catch (Exception $ex) {
        echo "Ao ao tentar inserir " . $ex->getMessage();
        
    }
    }
    
    public function setRedirect($redirect){
        $this->redirect = $redirect;
    }
    
    public function getRedirect(){
        return $this->redirect;
    }
    
    public function getPdo(){
        return $this->pdo;
    }

}
