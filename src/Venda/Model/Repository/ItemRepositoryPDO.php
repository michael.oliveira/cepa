<?php

namespace Venda\Model\Repository;

class ItemRepositoryPDO implements ItemRepository {
    private $pdo;
    protected $redirect;
    
    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }
    
    public function deleteItem($id){
        $stmt = $this->pdo->prepare("DELETE FROM item_venda WHERE cod_cliente = :id");
        $stmt->bindParam('id', $id);
        $stmt->execute();
    }
    
    public function setRedirect($redirect){
        $this->redirect = $redirect;
    }
    
    public function getRedirect(){
        return $this->redirect;
    }

    public function getItem($id) {
        $stmt = $this->pdo->prepare("SELECT * FROM item_venda WHERE cod_item = :id");
        $stmt->bindParam('id', $id);
        
        $stmt->execute();
        
        return $stmt->fetchObject('Venda\Model\ItemPedido');
    }

    public function getPedidoItems($id) {
        $stmt = $this->pdo->prepare("SELECT * FROM item_venda where cod_pedido = :id");
        $stmt->bindParam('id', $id);
        $stmt->setFetchMode(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE, 'Venda\Model\ItemPedido');
        $stmt->execute();
        
        $items = $stmt->fetchAll();

        $arr = array();
        $query = $this->pdo->prepare('select * from produto where cod_produto = :id');
        foreach($items as $item) {
            
            $query->setFetchMode(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE, 'Venda\Model\Produto');
            $query->execute(array('id' => $item->getProdutoId()));
            $produto = $query->fetch();
            $item->setProduto($produto);
            $arr[] = $item;
        }

        return $arr;
    }

    public function insertItem($dados) {
        try {
        
        $campos = implode(", ", array_keys($dados));
        
        $data = implode(',', array_map(function($x) {
            if(!is_float($x)){
                return "'" . $x . "'";
            } else {
                return $x;
            }
        }, $dados));
 
        $sql = "INSERT INTO item_venda ($campos) VALUES (" . $data. ")";
        $query = $this->pdo->prepare($sql);
        $query->execute();

    } catch (Exception $ex) {
        echo "Ao ao tentar inserir " . $ex->getMessage();
        
    }
    }

    public function updateItem($id, $dados) {
        try {
            $data = implode(", ", array_map(function($x, $v){
                
                return $x . " = \"" . $v . "\"";

            }, array_keys($dados), array_values($dados)));
            
            $sql = "UPDATE item_venda SET $data WHERE cod_item = :id";
            
            $query = $this->pdo->prepare($sql);
            $query->bindValue(':id', $id);
            $query->execute();

        } catch (Exception $ex) {
            echo "Ao ao tentar atualizar " . $ex->getMessage();
        
        }
    }

}
