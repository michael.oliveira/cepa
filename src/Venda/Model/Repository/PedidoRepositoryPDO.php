<?php

namespace Venda\Model\Repository;

use Venda\Model\ItemPedido;

class PedidoRepositoryPDO implements PedidoRepository {
    private $pdo;
    private $items = [];
    
    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
        $this->items = $this->restore();
    }
    
    public function restore(){
        return isset($_SESSION['pedido']) ? unserialize($_SESSION['pedido']) : array();
    }
    public function setCliente(Cliente $cliente){
        $this->cliente = $cliente;
    }
    
    public function getCliente(){
        return $this->cliente;
    }
    
    public function addItem(ItemPedido $item){
        $id = $item->getProduto()->getCodProduto();
        if (!$this->has($id)){
            $this->items[$id] = $item;
        }
    }

    public function removeItem($id) {
        if ($this->has($id)){
            unset($this->items[$id]);
        }
    }

    public function getPedidoItems() {
        return $this->items;
    }

    public function getTotal() {
        $total = 0;
        foreach($this->items as $item){
            $total += $item->getSubTotal();
        }
        return $total;
    }

    public function updateItem(ItemPedido $item) {
        $id = $item->getProduto()->getCodProduto();
        if ($this->has($id)){
            $this->items[$id] = $item;
        }
    }

    public function has($id) {
        return isset($this->items[$id]);
    }

    public function __destruct() {
        $_SESSION['pedido'] = serialize($this->items);
    }
    
    public function getPedido($id) {
        $stmt = $this->pdo->prepare("SELECT 
        pe.*,
        SUM(pr.preco_unitario * i.qtd) AS total
            FROM
                item_venda i
                JOIN pedido pe ON pe.cod_pedido = i.cod_pedido
                JOIN produto pr ON pr.cod_produto = i.cod_produto
                where pe.cod_pedido = :id
            GROUP BY pe.cod_pedido");
        $stmt->bindParam('id', $id);
        
        $stmt->execute();
        
        return $stmt->fetchObject('Venda\Model\Pedido');
    }

    public function getPedidos() {
        $stmt = $this->pdo->prepare("SELECT 
                pe.*,
                SUM(pr.preco_unitario * i.qtd) AS total
            FROM
                item_venda i
                JOIN pedido pe ON pe.cod_pedido = i.cod_pedido
                JOIN produto pr ON pr.cod_produto = i.cod_produto
            GROUP BY pe.cod_pedido");
        $stmt->setFetchMode(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE, 'Venda\Model\Pedido');
        $stmt->execute();
        
        $pedidos = $stmt->fetchAll();
        $arr = array();
        $query = $this->pdo->prepare('select * from cliente where cod_cliente = :id');
        foreach($pedidos as $pedido) {

            $query->setFetchMode(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE, 'Venda\Model\Cliente');
            $query->execute(array('id' => $pedido->getCodCliente()));
            $cliente = $query->fetch();
            $pedido->setCliente($cliente);
            $arr[] = $pedido;
        }
        
        return $arr;
    }

    public function deletePedido($id) {
        $stmt = $this->pdo->prepare("DELETE FROM pedido WHERE cod_pedido = :id");
        $stmt->bindParam('id', $id);
        $stmt->execute();
    }

    public function insertPedido($dados) {
        try {
        
        $campos = implode(", ", array_keys($dados));
        
        $data = implode(',', array_map(function($x) {
            if(!is_float($x)){
                return "'" . $x . "'";
            } else {
                return $x;
            }
        }, $dados));
 
        $sql = "INSERT INTO pedido ($campos) VALUES (" . $data. ")";
        $query = $this->pdo->prepare($sql);
        $query->execute();

        } catch (Exception $ex) {
            echo "Ao ao tentar inserir " . $ex->getMessage();  
        }   
    }
    
    public function limpaItems(){
        $this->items = [];
    }

    public function updatePedido($id, $dados) {
        try {
            $data = implode(", ", array_map(function($x, $v){
                
                return $x . " = \"" . $v . "\"";

            }, array_keys($dados), array_values($dados)));
            
            $sql = "UPDATE pedido SET $data WHERE cod_pedido = :id";
            
            $query = $this->pdo->prepare($sql);
            $query->bindValue(':id', $id);
            $query->execute();

        } catch (Exception $ex) {
            echo "Ao ao tentar inserir " . $ex->getMessage();
        
        }
    }
    
    public function getPdo(){
        return $this->pdo;
    }

}
