<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Venda\Model\Repository;

/**
 *
 * @author Michael
 */
interface ItemRepository {
    public function getPedidoItems($id);
    public function getItem($id);
    public function updateItem($id, $dados);
    public function insertItem($dados);
    public function deleteItem($id);
}
