<?php
namespace Venda\Model\Repository;

abstract class AbstractRepositoryPDO {
    private $pdo;
    protected $redirect;
    protected $entity;
    protected $class;
    protected $id;
    
    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }
   /*
    public function getClientes() {
        $stmt = $this->pdo->prepare("SELECT * FROM $this->entity");
        $stmt->setFetchMode(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE, $this->class);
        $stmt->execute();
        
        $produtos = $stmt->fetchAll();
        $arr = array();
        $query = $this->pdo->prepare('select * from tipo where cod_tipo = :id');
        foreach($produtos as $produto) {

            $query->setFetchMode(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE, 'Venda\Model\Tipo');
            $query->execute(array('id' => $produto->getTipoId()));
            $tipo = $query->fetch();
            $produto->setTipoProduto($tipo);
            $arr[] = $produto;
        }
        
        return $arr;
    }
    
    public function deleteCliente($id){
        $stmt = $this->pdo->prepare("DELETE FROM $this->entity 
                                     WHERE $this->id = :id");
        $stmt->bindParam('id', $id);
        $stmt->execute();
    }
    
    public function setRedirect($redirect){
        $this->redirect = $redirect;
    }
    
    public function getRedirect(){
        return $this->redirect;
    }

    public function getCliente($id) {
        $stmt = $this->pdo->prepare("SELECT * FROM $this->entity WHERE $this->id = :id");
        $stmt->bindParam('id', $id);
        
        $stmt->execute();
        
        return $stmt->fetchObject($this->class);
    }

    public function insertCliente($dados) {
        try {
        
        $campos = implode(", ", array_keys($dados));
        
        $data = implode(',', array_map(function($x) {
            if(!is_float($x)){
                return "'" . $x . "'";
            } else {
                return $x;
            }
        }, $dados));
 
        $sql = "INSERT INTO cliente ($campos) VALUES (" . $data. ")";
        $query = $this->pdo->prepare($sql);
        $query->execute();

    } catch (Exception $ex) {
        echo "Ao ao tentar inserir " . $ex->getMessage();
        
    }
    }

    public function updateCliente($id, $dados) {
        try {
            $data = implode(", ", array_map(function($x, $v){
                
                return $x . " = \"" . $v . "\"";

            }, array_keys($dados), array_values($dados)));
            
            $sql = "UPDATE cliente SET $data WHERE cod_produto = :id";
            
            $query = $this->pdo->prepare($sql);
            $query->bindValue(':id', $id);
            $query->execute();

        } catch (Exception $ex) {
            echo "Ao ao tentar inserir " . $ex->getMessage();
        
        }
    }
    * 
    */
}
