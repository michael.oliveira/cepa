<?php

namespace Venda\Model\Repository;

use Venda\Model\ItemPedido;

interface PedidoRepository {
    public function getPedidos();
    public function getPedido($id);
    public function updatePedido($id, $dados);
    public function insertPedido($dados);
    public function deletePedido($id);
    public function addItem(ItemPedido $item);
    public function updateItem(ItemPedido $item);
    public function removeItem($id);
    public function has($id);
    public function getTotal();
    public function getPedidoItems();
    public function getPdo();
    public function limpaItems();
}
