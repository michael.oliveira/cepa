<?php

namespace Venda\Model\Repository;

interface ProdutoRepository {
    public function getProdutos();
    public function getProduto($id);
    public function updateProduto($id, $dados);
    public function insertProduto($dados);
    public function delete($id);
}
