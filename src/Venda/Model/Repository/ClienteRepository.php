<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Venda\Model\Repository;

/**
 *
 * @author Michael
 */
interface ClienteRepository {
    public function getClientes();
    public function getCliente($id);
    public function updateCliente($id, $dados);
    public function insertCliente($dados);
    public function deleteCliente($id);
}
