<?php

namespace Venda\Model;

use Mvc\Database;

abstract class Model {
    protected $table;
    protected $primaryKey = "id";
    
    public static function find($id){
        $model = new static();
        
        $sql = "SELECT 
                *
                FROM " .
                $model->table .
                " WHERE " .
                $model->primaryKey . " = :id";

        
        $params = ["id" => $id];
        $result = Database::query($sql, $params);
        
        return $result;
    }
    
    public static function delete($id){
        $model = new static();
        try {
        $sql = "DELETE
                FROM " .
                $model->table .
                " WHERE " .
                $model->primaryKey . " = :id";

        $query = Database::connection()->prepare($sql);
        $query->bindValue('id', $id);
        $query->execute();

        } catch (Exception $ex) {
            echo "Ao ao tentar inserir " . $ex->getMessage();
        
        }
    }
    
    public static function findAll($sql){

        $result = Database::fetchAll($sql);
        
        return $result;
    }
    public static function insert($table, $data){
        Database::insert($table, $data);
    }
    
    public static function update($table, $id, $params = []){
        $model = new static();
        try {
            
            
            $data = implode(", ", array_map(function($x, $v){
                
                return $x . " = \"" . $v . "\"";

            }, array_keys($params), array_values($params)));
            
            $sql = "UPDATE $table SET $data WHERE " . $model->primaryKey . " = :id";
            
            $query = Database::connection()->prepare($sql);
            $query->bindValue(':id', $id);
            $query->execute();

        } catch (Exception $ex) {
            echo "Ao ao tentar inserir " . $ex->getMessage();
        
        }
    }
}
