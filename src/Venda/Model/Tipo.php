<?php

namespace Venda\Model;

class Tipo {
    private $cod_tipo;
    private $descricao;
    
    public function getCodTipo() {
        return $this->cod_tipo;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function setCodTipo($cod_tipo) {
        $this->cod_tipo = $cod_tipo;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }


}
