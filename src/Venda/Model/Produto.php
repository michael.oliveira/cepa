<?php
namespace Venda\Model;

class Produto extends Model {

    protected $cod_produto;
    private $cod_tipo_produto;
    private $tipoProduto;
    private $descricao;
    private $qtd_estoque;
    private $preco_unitario;

    function getCodProduto() {
        return $this->cod_produto;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getQtdEstoque() {
        return $this->qtd_estoque;
    }

    public function getPrecoUnitario() {
        return $this->preco_unitario;
    }
    
    public function setCodProduto($codProduto) {
        $this->codProduto = $codProduto;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setQtdEstoque($qtdEstoque) {
        $this->qtdEstoque = $qtdEstoque;
    }

    public function setPrecoUnitario($precoUnitario) {
        $this->preco_unitario = $precoUnitario;
    }

    public function getTipoId(){
        return $this->cod_tipo_produto;
    }
    
    public function getTipoProduto(){
        return $this->tipoProduto;
    }
    
    public function setTipoProduto(Tipo $tipo){
        $this->tipoProduto = $tipo;
    }
            
}
