<?php

namespace Venda\Model;

use Venda\Model\Cliente;

class Pedido {
    
    private $cod_pedido;
    private $data_pedido;
    private $cod_cliente;
    private $cliente;
    private $total;

    public function getCodPedido() {
        return $this->cod_pedido;
    }

    public function getDataPedido() {
        return $this->data_pedido;
    }

    public function getCodCliente() {
        return $this->cod_cliente;
    }

    public function getCliente() {
        return $this->cliente;
    }

    public function setCodPedido($cod_pedido) {
        $this->cod_pedido = $cod_pedido;
    }

    public function setDataPedido($data_pedido) {
        $this->data_pedido = $data_pedido;
    }

    public function setCodCliente($cod_cliente) {
        $this->cod_cliente = $cod_cliente;
    }

    public function setCliente(Cliente $cliente) {
        $this->cliente = $cliente;
    }
    
    public function getTotal(){
        return $this->total;
    }

}
