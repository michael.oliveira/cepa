<?php

namespace Venda\Model;

use Venda\Model\Produto;

class ItemPedido extends Model {
    private $cod_item;
    private $produto;
    private $cod_produto;
    private $qtd;
    
    public function getProdutoId(){
        return $this->cod_produto;
    }

    public function setCodItem($codItem){
        $this->cod_item = $codItem;
    }
    
    public function getCodItem(){
        return $this->cod_item;
    }
    
    public function setProduto(Produto $produto){
        $this->produto = $produto;
    }
    
    public function getProduto(){
        return $this->produto;
    }
    
    public function setQtd($qtd){
        $this->qtd = $qtd;
    }
    
    public function getQtd(){
        return $this->qtd;
    }
    
    public function getSubTotal(){
        return $this->produto->getPrecoUnitario() * $this->qtd;
    }
}
