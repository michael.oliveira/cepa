<div class="container">
   
        <div class="panel panel-primary">
            <div class="panel-heading">Lista de Clientes</div>
            <div class="panel-body">
                <table class="table table-striped">
        <thead>
            <th>Código</th>
            <th>Cliente</th>
            <th>Endereço</th>
            <th>Bairro</th>
            <th>Cidade</th>
            <th>Ações</th>
        </thead>
        <tbody>
            <?php foreach($rows as $cliente): ?> <!-- Pega todos os registros de pedidos do banco de dados atribuidos a variavel $rows e passa para a $row exibir linha por linha  -->
            <tr>
                <td><?= $cliente->getCodCliente(); ?></td>
                <td><?= $cliente->getNome(); ?></td>
                <td><?= $cliente->getEndereco(); ?></td>
                <td><?= $cliente->getBairro(); ?></td>
                <td><?= $cliente->getCidade(); ?></td>
                <td>
                    <a href="/venda/public/cliente/edit/<?= $cliente->getCodCliente() ?>" class="glyphicon glyphicon-pencil"></a>
                    <a href="#" 
                        onclick="confirm(
                                    'Deletar Cliente', 
                                    'Tem certeza que deseja excluir o(a) cliente ' +
                                    '<?= $cliente->getNome() ?> ?',
                                    'Fechar',
                                    'Deletar',
                                    null,
                                    '/venda/public/cliente/delete/'+ <?= $cliente->getCodCliente() ?>)"
                        class="glyphicon glyphicon-trash text-danger"
                    ></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
            </div>
            <div class="panel-footer">
                <a href="cliente/add" class="btn btn-primary">Novo Cliente</a>
            </div>
        </div>
    
    </div>
