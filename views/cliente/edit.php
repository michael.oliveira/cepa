<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Editar Cliente</div>
        <form method="post" action="<?= $redirect . "/save/" . $id?>" class="form-horizontal">
        <div class="panel-body">
            <div class="form-group">
                <label for="nome" class="col-sm-2 control-label">Nome</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Entre com o nome do cliente" name="nome"
                           value="<?= $cliente->getNome() ?>">
                </div>
            </div>
                <div class="row" style="padding-bottom: 15px;">

                <label for="cpf" class="col-xs-2 control-label">CPF</label>
                <div class="col-xs-3">
                    <input type="text" class="form-control" placeholder="CPF" name="cpf" value="<?= $cliente->getCpf() ?>">
                </div>
                    <label for="rg" class="col-xs-1 control-label">RG</label>
                <div class="col-xs-2">
                    <input type="text" class="form-control" placeholder="RG" name="rg" value="<?= $cliente->getRg() ?>">
            </div>
            </div>
            <div class="form-group">
                <label for="endereco" class="col-sm-2 control-label">Endereço</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Endereço completo, com numero" 
                           name="endereco" value="<?= $cliente->getEndereco() ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="bairro" class="col-sm-2 control-label">Bairro</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Bairro" name="bairro"
                           value="<?= $cliente->getBairro() ?>">
                </div>
            </div>
            <div class="row">
                <label for="cidade" class="col-xs-2 control-label">Cidade</label>
                <div class="col-xs-3">
                    <input type="text" class="form-control" placeholder="Cidade" name="cidade"
                           value="<?= $cliente->getCidade() ?>">
                </div>
                    <label for="bairro" class="col-xs-1 control-label">UF</label>
                <div class="col-xs-2">
                    <input type="text" class="form-control" placeholder="UF" name="uf"
                           value="<?= $cliente->getUf() ?>">
                </div>
            </div>
                    </div>
                    <div class="panel-footer">
                    <input type="submit" value="Salvar" class="btn btn-primary">
                    <a href="/venda/public/cliente" class="btn btn-default">Voltar</a>
                </div>
            </form> 
        </div>
    
</div>