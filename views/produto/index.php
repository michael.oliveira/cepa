<div class="container">
        <h2>Lista de Produtos</h2>
   
        <div class="panel panel-default">
            <div class="panel-heading">  
                <a href="/venda/public/produto/add" class="btn btn-primary">Novo Produto</a>
            
            </div>
            <div class="panel-body">
                <table class="table table-striped">
        <thead>
            <th>Código</th>
            <th>Descrição</th>
            <th>Tipo</th>
            <th>Preço</th>
            <th>Estoque</th>
            <th>Ações</th>
        </thead>
        <tbody>
            <?php foreach($rows as $produto): ?> <!-- Pega todos os registros de pedidos do banco de dados atribuidos a variavel $rows e passa para a $row exibir linha por linha  -->
            <tr>
                <td><?= $produto->getCodProduto(); ?></td>
                <td><?= $produto->getDescricao(); ?></td>
                <td><?= $produto->getTipoProduto()->getDescricao(); ?></td>
                <td>R$ <?= number_format($produto->getPrecoUnitario(), 2, ",", "."); ?></td>
                <td><?= $produto->getQtdEstoque(); ?></td>
                <td>
                    <a href="/venda/public/produto/edit/<?= $produto->getCodProduto() ?>" class="glyphicon glyphicon-pencil"></a>
                    <a href="/venda/public/pedido/add/<?= $produto->getCodProduto() ?>" class="glyphicon glyphicon-import" alt="Adicionar no carrinho"></a>
                    <a href="#" 
                        onclick="confirm(
                                    'Deletar Produto', 
                                    'Tem certeza que deseja excluir o produto ' +
                                    '<?= $produto->getDescricao() ?> ?',
                                    'Fechar',
                                    'Deletar',
                                    null,
                                    '/venda/public/produto/delete/'+ <?= $produto->getCodProduto() ?>)"
                        class="glyphicon glyphicon-trash text-danger"
                    ></a>
                </td>
            </tr>
                <?php endforeach; ?>
            </tbody>
            </table>
        </div>
    </div>
</div>