<div class="container">
    <div class="panel panel-default">
        
        <div class="panel-heading"><h3>Editar de Produto</h3></div>
        <form method="post" action="../save/<?= $id ?>" class="form-horizontal">
        <div class="panel-body">
            
            <div class="form-group">
                <label for="nome" class="col-sm-2 control-label">Descrição</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Entre com a descrição" 
                           value="<?= $produto->getDescricao() ?>"
                           name="descricao">
                </div>
            </div>
            <div class="form-group">
                <label for="nome" class="col-sm-2 control-label">Tipo</label>
                <div class="col-sm-6">
                    <select name="cod_tipo_produto" class="form-control">
                        <?php foreach($tipos as $tipo): ?>
                        <option value="<?= $tipo['cod_tipo'] ?>" 
                            <?= (($tipo['cod_tipo'] == $produto->getTipoId())) ? "selected" : ""; ?>>
                        <?= $tipo['descricao'] ?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
                <div class="row" style="padding-bottom: 15px;">

                <label for="cpf" class="col-xs-2 control-label">Preço Unitário:</label>
                <div class="col-xs-3">
                    <input type="text" class="form-control" placeholder="Preço" 
                           value="<?= $produto->getPrecoUnitario() ?>"
                           name="preco_unitario" size="20">
                </div>
                    <label for="rg" class="col-xs-1 control-label">Estoque</label>
                <div class="col-xs-2">
                    <input type="text" class="form-control" placeholder="Qtd Estoque" 
                           value="<?= $produto->getQtdEstoque() ?>"
                           name="qtd_estoque">
            </div>
            </div>
                    </div>
                    <div class="panel-footer">
                    <input type="submit" value="Salvar" class="btn btn-success">
                </div>
            </form> 
        </div>
    
</div>