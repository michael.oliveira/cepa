<div class="container">
    <div class="panel panel-primary">
        
        <div class="panel-heading">Cadastro de Produto</div>
        <form method="post" action="save" class="form-horizontal">
        <div class="panel-body">
            <div class="form-group">
                <label for="nome" class="col-sm-2 control-label">Descrição</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Entre com a descrição" name="descricao">
                </div>
            </div>
            <div class="form-group">
                <label for="nome" class="col-sm-2 control-label">Tipo</label>
                <div class="col-sm-6">
                    <select name="cod_tipo_produto" class="form-control">
                        <?php foreach($tipos as $tipo): ?>
                        <option value="<?= $tipo['cod_tipo'] ?>">
                        <?= $tipo['descricao'] ?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
                <div class="row" style="padding-bottom: 15px;">

                <label for="cpf" class="col-xs-2 control-label">Preço Unitário:</label>
                <div class="col-xs-3">
                    <input type="text" class="form-control" placeholder="Preço" name="preco_unitario" size="20">
                </div>
                    <label for="rg" class="col-xs-1 control-label">Estoque</label>
                <div class="col-xs-2">
                    <input type="text" class="form-control" placeholder="Qtd Estoque" name="qtd_estoque">
            </div>
            </div>
                    </div>
                    <div class="panel-footer">
                    <input type="submit" value="Salvar" class="btn btn-success">
                </div>
            </form> 
        </div>
    
</div>