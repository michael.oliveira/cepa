<div class="container">
    <div class="panel panel-default">
        
            <div class="panel-heading"><i class="glyphicon glyphicon-ok"></i> Novo Item</div>
            <form method="post" action="pedido/buscarProduto" class="form-horizontal" id="frm_loc_prod">
        <div class="panel-body">
            
            <div class="form-group">
                <div class="col-md-1 control-label">
                    <input type="checkbox" name="criterio[]" value="cod_produto">
                </div>
                <label for="nome" class="col-sm-1 control-label">Código</label>
                
                <div class="col-sm-2">
                    <input type="text" class="form-control" placeholder="Código do Produto" name="cod_produto" size="20">
                </div>
            </div>
                <div class="row">
                <div class="col-md-1 control-label">
                    <input type="checkbox" name="criterio[]" value="descricao">
                </div>
                <label for="cpf" class="col-md-1 control-label">Descrição</label>
                <div class="col-md-3">
                    <input type="text" class="form-control" placeholder="Descricao" name="descricao">
                </div>
                    <label for="rg" class="col-md-1 control-label">Qtd</label>
                <div class="col-md-2">
                    <input type="number" class="form-control" placeholder="Qtd" name="qtd">
                </div>
            </div>
                    </div>
                    <div class="panel-footer">
                        <input type="submit" value="Buscar" class="btn btn-primary">
                    <a href="concluir" class="btn btn-success">Terminar</a>
                </div>
            </form> 
        </div>
    
    <div class="panel panel-default">
            
            <div class="panel-heading">Itens da Venda</div>
                <div class="panel-body">
                    <?php 
            if(empty($rows)): ?>
            <h3 class="danger">Nenhum Item no Carrinho</h3>
            <?php 
            else: 
            
            ?>
                    <table class="table table-striped">
        <thead>
            <th></th>
            <th>Código</th>
            <th>Descrição</th>
            <th>Preço</th>
            <th>Qtd</th>
            <th>Total</th>
        </thead>
        <tbody>
            
            <?php foreach($rows as $row): ?> <!-- Pega todos os registros de pedidos do banco de dados atribuidos a variavel $rows e passa para a $row exibir linha por linha  -->
            <tr>
                <td><a href="delete/<?= $row['cod_item'] ?>"><i class="glyphicon glyphicon-minus-sign text-danger"</a></td>
                <td><?= $row['cod_produto']; ?></td>
                <td><?= $row['descricao'] ?></td>
                <td><?= $row['preco_unitario']; ?></td>
                <td><?= $row['qtd']; ?></td>
                <td><?= $row['total']; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
            <?php endif; ?>
                </div>
            </div> 
</div>
<div class="recebe_dados"></div>
<script src="/venda/public/js/incluir_item.js" type="text/javascript"></script>