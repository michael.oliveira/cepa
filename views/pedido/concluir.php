<div class="container">
 <form action="/venda/public/pedido/save" method="post">
            
            <div class="panel panel-primary">
                <div class="panel panel-heading">
                Finalizar Venda
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <div class="col-sm-2 control-label">
                                Cliente:
                            </div>
                            <div class="col-sm-8">
                                <select name="cod_cliente">
                                    <?php foreach($clientes as $cliente): ?>
                                    <option value="<?= $cliente->getCodCliente() ?>">
                                        <?= $cliente->getNome() ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                
                </div>
                <div class="panel panel-footer">
                    <input type="submit" class="btn btn-primary" value="Salvar">
                </div>
        </div>
     </form>
        <div class="panel panel-default">

            <div class="panel panel-heading">
                Relatorio de Venda
                    
            </div>
            <div class="panel-body">
                  <table class="table table-striped">
            <thead>
                <th width="80">Código</th>
                <th width="500">Produto</th>
                <th>Quantidade</th>
                <th>Preço</th>
                <th>SubTotal</th>
            
            </thead>
            <tbody>
            <?php foreach($pedido->getPedidoItems() as $item): ?> <!-- Pega todos os registros de pedidos do banco de dados atribuidos a variavel $rows e passa para a $row exibir linha por linha  -->
            <tr>
                <td><?= $item->getProduto()->getCodProduto(); ?></td>
                <td><?= $item->getProduto()->getDescricao(); ?></td>
                <td><?= $item->getQtd() ?></td>
                <td>R$ <?= $item->getProduto()->getPrecoUnitario(); ?></td>
                <td>R$ <?= number_format($item->getSubTotal(), 2, ",", ".") ; ?></td>
                
            </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="4" style="text-align: right"><b>Total</b></td>
                <td>
                    <b>R$ <?= number_format($pedido->getTotal(), 2, ",", ".") ?></b>
                </td>
            </tr>
        </tbody>
        </table>      
    </div> 
    </div>
</div>