<div class="container">
        <h3><i class="glyphicon glyphicon-th-list"></i>  - Itens da Venda</h3>
 
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <a href="/venda/public/produto" class="btn btn-primary">
                            <i class="glyphicon glyphicon-check"></i> Buscar Produto</a>
               <?php if (count($rows->getPedidoItems()) > 0): ?>
                    <a href="/venda/public/pedido/concluir" class="btn btn-primary">
                            <i class="glyphicon glyphicon-ok"></i> Finalizar Venda</a>
                    
                <a href="/venda/public/pedido/limpar" class="btn btn-danger">
                            <i class="glyphicon glyphicon-remove"></i> Excluir Todos</a>
                            <?php endif; ?>
            </div>
            <div class="panel-body">
                <?php if (count($rows->getPedidoItems()) > 0): ?>
                  <table class="table table-striped">
        <thead>
            <th width="50"></th>
            <th width="80">Código</th>
            <th width="500">Produto</th>
            <th>Quantidade</th>
            <th>Preço</th>
            <th>SubTotal</th>
            
        </thead>
        <tbody>
            <?php 
            
                foreach($rows->getPedidoItems() as $item): 
            ?> <!-- Pega todos os registros de pedidos do banco de dados atribuidos a variavel $rows e passa para a $row exibir linha por linha  -->
            <tr>
                <td>
                    <a href="#" 
                        onclick="confirm(
                                    'Deletar Item', 
                                    'Tem certeza que deseja excluir o Item ' +
                                    '<?= $item->getProduto()->getDescricao() ?> ?',
                                    'Fechar',
                                    'Deletar',
                                    null,
                                    '/venda/public/pedido/removeitem/' + <?= $item->getProduto()->getCodProduto() ?>)"
                        class="glyphicon glyphicon-trash text-danger"
                    ></a>
                </td>
                <td><?= $item->getProduto()->getCodProduto(); ?></td>
                <td><?= $item->getProduto()->getDescricao(); ?></td>
                <td>
                    <form action="<?= $redirect . "/update" ?>" method="post">
                        <input type="hidden" value="<?= $item->getProduto()->getCodProduto() ?>" name="cod_produto">
                        <input size="10" type="number" value="<?= $item->getQtd() ?>" style="width: 50px;" name="qtd">
                        <button type="submit" class="btn btn-primary btn-xs">Alterar</button>
                    </form>
                </td>
                <td>R$ <?= $item->getProduto()->getPrecoUnitario(); ?></td>
                <td>R$ <?= number_format($item->getSubTotal(), 2, ",", ".") ; ?></td>
                
            </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="5" style="text-align: right"><b>Total</b></td>
                <td>
                    <b>R$ <?= number_format($rows->getTotal(), 2, ",", ".") ?></b>
                </td>
            </tr>
            
        </tbody>
        </table>  
                <?php else: ?>
                <h2>Nenhum Item Selecionado</h2>
            <?php endif; ?>
    </div> 
    </div>
</div>