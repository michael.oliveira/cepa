<div class="container">
        <h3><i class="glyphicon glyphicon-new-window"></i>  - Balção de Venda</h3>
 
        <div class="panel panel-default">
            <div class="panel panel-heading"><a href="/venda/public/pedido/items" class="btn btn-primary">
                            <i class="glyphicon glyphicon-check"></i> Nova Venda</a></div>
            <div class="panel-body">
                  <table class="table table-striped">
        <thead>
            <th></th>
            <th>Código</th>
            <th>Cliente</th>
            <th>Data</th>
            <th>Valor</th>
        </thead>
        <tbody>
            <?php foreach($pedidos as $pedido): ?> <!-- Pega todos os registros de pedidos do banco de dados atribuidos a variavel $rows e passa para a $row exibir linha por linha  -->
            <tr>
                <td>

                    <a href="#" 
                        onclick="confirm(
                                    'Deletar Pedido', 
                                    'Tem certeza que deseja excluir o(a) o Pedido ' +
                                    '<?= $pedido->getCliente()->getNome() ?> ?',
                                    'Fechar',
                                    'Deletar',
                                    null,
                                    '/venda/public/pedido/delete/' + <?= $pedido->getCodPedido() ?>)"
                        class="glyphicon glyphicon-trash text-danger"
                    ></a>
                    <a href="/venda/public/pedido/exibir/<?= $pedido->getCodPedido() ?>" class="glyphicon glyphicon-print"></a>
                </td>
                <td><?= $pedido->getCodPedido(); ?></td>
                <td><?= $pedido->getCliente()->getNome(); ?></td>
                <td><?= \DateTime::createFromFormat("Y-m-d", $pedido->getDataPedido())->format("d/m/Y"); ?></td>
                <td>R$ <?= number_format($pedido->getTotal(), 2, ",", "."); ?></td>
                
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>      
    </div> 
    </div>
</div>