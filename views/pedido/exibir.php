<div class="container">
            
            <div class="panel panel-primary">
                <div class="panel panel-heading">
                Dados da Venda
                </div>
                <div class="panel-body">
                    <div class="row">

                            <div class="col-sm-1 control-label">
                                <b>Código:</b>
                            </div>
                            <div class="col-sm-1">
                                <?= $pedido->getCodPedido() ?>
                            </div>
                        
                            <div class="col-sm-1 control-label">
                                <b>Cliente:</b>
                            </div>
                            <div class="col-sm-5">
                                <?= $cliente->getNome() ?>
                            </div>

                            <div class="col-sm-1 control-label">
                                <b>Data:</b>
                            </div>
                            <div class="col-sm-3">
                                <?= \DateTime::createFromFormat("Y-m-d", $pedido->getDataPedido())->format("d/m/Y") ?>
                            </div>
                        </div>
                    </div>
                </div>


        <div class="panel panel-default">

            <div class="panel panel-heading">
                Descrição dos Itens
                    
            </div>
            <div class="panel-body">
                  <table class="table table-striped">
            <thead>
                <th width="80">Código</th>
                <th width="500">Produto</th>
                <th>Quantidade</th>
                <th>Preço</th>
                <th>SubTotal</th>
            
            </thead>
            <tbody>
            <?php foreach($items as $item): ?> <!-- Pega todos os registros de pedidos do banco de dados atribuidos a variavel $rows e passa para a $row exibir linha por linha  -->
            <tr>
                <td><?= $item->getProduto()->getCodProduto(); ?></td>
                <td><?= $item->getProduto()->getDescricao(); ?></td>
                <td><?= $item->getQtd() ?></td>
                <td>R$ <?= $item->getProduto()->getPrecoUnitario(); ?></td>
                <td>R$ <?= number_format($item->getSubTotal(), 2, ",", ".") ; ?></td>
                
            </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="4" style="text-align: right"><b>Total</b></td>
                <td>
                    <b>R$ <?= number_format($pedido->getTotal(), 2, ",", ".") ?></b>
                </td>
            </tr>
        </tbody>
        </table>      
    </div> 
    </div>
</div>