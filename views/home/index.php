<!-- <?php phpinfo(); ?> -->
<div class="container">
        <div class="panel panel-primary">
            
                <div class="panel-heading">Acesso Rápido</div>
            <div class="panel-body">
                <a href="<?= $redirect . "pedido/items" ?>" class="btn btn-primary"> Novo Pedido</a>
                <a href="<?= $redirect  . "cliente/add" ?>" class="btn btn-info">Novo Cliente</a>
            </div>
        </div>
        <div class="panel panel-default">
            
            <div class="panel-heading">Ultimas Vendas</div>
                <div class="panel-body">
                    <table class="table table-striped">
        <thead>
            <th>Código</th>
            <th>Data</th>
            <th>Cliente</th>
            <th>Endereço</th>
            <th>Bairro</th>
            <th>Cidade</th>
        </thead>
        <tbody>
            <?php foreach($rows as $pedido): ?> <!-- Pega todos os registros de pedidos do banco de dados atribuidos a variavel $rows e passa para a $row exibir linha por linha  -->
            <tr>
                <td><?= $pedido->getCodPedido(); ?></td>
                <td>
                <?php 
                    $data = \DateTime::createFromFormat("Y-m-d", $pedido->getDataPedido());
                    echo $data->format("d/m/Y"); 
                ?>
                </td>
                <td><?= $pedido->getCliente()->getNome(); ?></td>
                <td><?= $pedido->getCliente()->getEndereco(); ?></td>
                <td><?= $pedido->getCliente()->getBairro(); ?></td>
                <td><?= $pedido->getCliente()->getCidade(); ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
                </div>
            </div>
    </div>